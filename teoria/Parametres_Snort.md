# Parametres de Snort

Els diferents parametres que podem incloure a l'hora de executar Snort ens permeten iniciar aquest programa amb diferents funcions i accions a realitzar.
També ens permet indicar on es troben els diferents fitxers de configuració segons com vulguem executar-lo.

En aquest apartat apareixera la definició dels parametres utilitzats per dur a terme aquest projecte i d'altres interesants que ens poden servir d'ajuda.

## Parametres

- -A console &rightarrow; ens permet indicar a Snort que ens printi les alarmes per pantalla.

- -b &rightarrow; desar els paquets rebuts en fitxer tcpdump.

- -c fitxer-config &rightarrow; ens permet indicar on es troba el fitxer de configuració que volem fer servir.

- -l directori-registres &rightarrow; ens permet definir on volem desar els nostres registres.

- -D &rightarrow; iniciar Snort com a dimoni. Les alertes seran desades a /var/log/snort/alert si no es diu el contrari.

- -g grup &rightarrow; canvia el grup amb el qual Snort s'executa després de inicialitzar-se. Això ens permet treure-li els privilegis de root.

- -u usuari &rightarrow; canvia el usuariamb el qual Snort s'executa després de inicialitzar-se.

- -i &rightarrow; interficie per la qual Snort ha d'actuar.

- -K mode-de-registre &rightarrow; ens permet indicar en quin tipus de format volem desar els nostres registres.

- -N &rightarrow; desactiva el registre de paquets. Seguira generan alertes.

- -Q &rightarrow; activa el mode inline, el qual ens permet fer servir Snort com a IPS.

- -U &rightarrow; canvia la marca de temps a UTC de tots els registres.

- -V &rightarrow; mostra la versió.

- -h &rightarrow; mostra l'ajuda.
