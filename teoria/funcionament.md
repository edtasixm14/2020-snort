# Funcionament de Snort

El motor de Snort es divideix en les següents parts:
- Descodificador del paquet
- Preprocesadors
- Motor de detecció 
- Plugins de sortida

El descodificador del paquet, agafa el paquet de la interficie assignada, i el prepara per ser preprocessat o enviat al motor de detecció.

Els preprocesadors són componentes o plugins que pueden ser utilitzats amb Snort per identificar paquets abans de que el motor de detección fagi alguna operació per trobar si el paquet esta sent enviat per un intrus. Són molt importants ja que preparen les dades per ser analitzades contra les regles del motor de detecció.

El motor de detecció és l'encarregat de mirar els paquets i comprovar si concideixen amb alguna regla. I així doncs actua en conseqüència.

Els plugins de sortida permeten emmagatzemar les alertes i els registres en diferents formats.

![](../aux/snort_esquema.png)
